let data = require("../assets/posts.json");
let _ = require("lodash");
const fs = require('fs');

// serving the responses
module.exports = {
    getData: function () {
        return data;
    }, removeFavorite: function (userId) {
        setData(userId, false, data);
    }, addFavorite: function (userId) {
        setData(userId, true, data);
    }
};

// add/remove functionality
function setData(id, isFavourite, arr) {
    _.forEach(arr, function(value, key) {
        if(arr[key]["uid"] === id){
            arr[key]["favourite"] = isFavourite;
        }
    });
    data = arr;
    updateJSON(arr);
}

// file manipulator
function updateJSON(arr) {
    const jsonString = JSON.stringify(arr);
    fs.writeFile('./assets/posts.json', jsonString, err => {
        if (err) {
            console.log('Error in adding the record', err)
        } else {
            console.log('Successfully added the record')
        }
    });
}