let express = require("express");
let cors = require('cors');
let bodyParser = require("body-parser");
let index  = express();
let port = 8000;
let data = require("./controller/controller");

index.use(cors());
index.use(bodyParser.json());
index.use(bodyParser.urlencoded({ extended: true }));

// to get the data
index.get('/', (req, res) => {
    res.send(data.getData());
});

// to add to favorites
index.post('/favorites', function (req, res) {
    console.log("Receiving ID " + req.body["userId"]);
    data.addFavorite(req.body["userId"]);
    res.send("successfully added to favorites!");
});

// to remove from favorites
index.post('/remove', function (req, res) {
    console.log("Receiving ID " + req.body["userId"]);
    data.removeFavorite(req.body["userId"]);
    res.send("successfully removed from favorites!");
});

// initializing the port
index.listen(port, () => {
    console.log(`FoodEx-Server listening at http://localhost:${port}`)
});
