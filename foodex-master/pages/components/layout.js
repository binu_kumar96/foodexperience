import Header from "./header";
import BottomNav from "./bottom-nav";
import TopNav from "./top-nav";
import styled from "styled-components";
import Head from "next/head";
import { useState, useEffect } from 'react';

const HeaderWrapper = styled.div`
    max-width: 50rem;
    padding: 0 1rem;
    margin: 0rem auto 1rem;
    z-index: 1;
    position: fixed;
    top: 0px;
    left: 0px;
    right: 0px;
    @media screen and (min-width: 480px) {
        margin: 0rem auto 1rem;
    }
`;

const TopNavWrapper = styled.div`
    max-width: 40rem;
    margin: 5rem auto 1rem;
    position: fixed;
    z-index: 1;
    top: 0px;
    left: 0px;
    right: 0px;
`;

const BodyWrapper = styled.div`
    max-width: 50rem;
    padding: 0 1rem;
    margin: 7rem auto 6rem;
    @media screen and (min-width: 480px) {
        margin: 10rem auto 10rem auto;
        max-width: 50rem;
        min-width: 20rem;
        position: relative;
        left: 10rem;
        right: 10rem;
    }
`;

const BottomNavWrapper = styled.div`
    max-width: 50rem;
    position: fixed;
    bottom: 0px;
    padding: 10px;
    margin: 0rem auto 0rem;
    left: 0px;
    right: 0px;
    background-color: palevioletred;
`;

function useWindowSize() {

    const [windowSize, setWindowSize] = useState({
        width: undefined,
    });

    useEffect(() => {
        function handleResize() {
            setWindowSize({
                width: window.innerWidth
            });
        }

        window.addEventListener("resize", handleResize);

        handleResize();

        return () => window.removeEventListener("resize", handleResize);
    }, []);

    return windowSize;
}

export default function Layout({ children, title="FoodEx", description="Food Experience Application" }) {
    const size = useWindowSize();
    return (
        <>
            <Head>
                <title>{title}</title>
                <meta
                    name="description"
                    content={description}
                />
            </Head>
            {
                size.width > 480 ? (
                    <>
                        <HeaderWrapper>
                            <Header/>
                        </HeaderWrapper>
                        <TopNavWrapper>
                            <TopNav />
                        </TopNavWrapper>
                        <BodyWrapper>
                            { children }
                        </BodyWrapper>
                    </>
                ) : (
                    <>
                        <HeaderWrapper>
                            <Header/>
                        </HeaderWrapper>
                        <BodyWrapper>
                            { children }
                        </BodyWrapper>
                        <BottomNavWrapper>
                            <BottomNav/>
                        </BottomNavWrapper>
                    </>
                )
            }
        </>
    )
}