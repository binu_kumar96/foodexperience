import styled from "styled-components";
import { FastFood } from "@styled-icons/ionicons-outline/FastFood";

const DarkFastFood = styled(FastFood)`
  color: black;
  margin-bottom: 10px;
`;

const Title = styled.div`
  background-color: #fff;
  border-bottom: 1px solid rgba(0, 0, 0, 0.0975);
`;

const TitleHeader = styled.div`
  max-width: 1010px;
  padding: 15px 10px;
  width: 100%;
  display: flex;
  align-items: center;
  margin: 0 auto;
`;

const TitleCenter = styled.div`
  width: 95%;
  text-align: center;
  font-family: Jazz LET, fantasy;
  font-size: 30px;  
`;

const Header = () => (
     <Title>
        <TitleHeader>
            <TitleCenter>
                FoodEx | <DarkFastFood size="35"/>
            </TitleCenter>
        </TitleHeader>
    </Title>
);

export default Header;