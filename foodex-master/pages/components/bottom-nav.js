import styled, {ThemeProvider} from "styled-components";
import { HomeFill } from "@styled-icons/octicons/HomeFill";
import { Heart } from "@styled-icons/boxicons-solid/Heart";
import Link from "next/link";

const LightHomeFill = styled(HomeFill)`
    color: white;
`;

const LightHeart = styled(Heart)`
    color: white;
`;

const theme = {
    fg: "palevioletred",
    bg: "white"
};

const ButtonWrapper = styled.div`
    display: flex;
    width: 100%;
    justify-content: ${({ page }) => {
    if (page === 'first') return 'flex-end';
    else if (page === 'middle') return 'space-between';
    else return 'flex-start';
}};
`;

const Button = styled.button`
    color: ${props => props.theme.bg};
    border: 2px solid ${props => props.theme.bg};
    background: ${props => props.theme.fg};
    font-size: 1em;
    padding: 10px;
    position: relative;
    border: none;
    width: 48%; 
`;

const BottomNav = () => (
     <ThemeProvider theme={theme}>
        <ButtonWrapper page="middle">
            <Link href="/" passHref>
                <Button>
                    <LightHomeFill size="25"/>
                </Button>
            </Link>
            <Link href="/favorites" passHref>
                <Button>
                    <LightHeart size="25"/>
                </Button>
            </Link>
        </ButtonWrapper>
    </ThemeProvider>
);

export default BottomNav;