import styled from "styled-components";
import Link from "next/link";

const NavWrapper = styled.div`
    background-color: palevioletred;
    color: #f2f2f2;
    text-align: center;
    padding: 14px 5px;
    text-decoration: none;
    font-size: 17px;
    left: 0;
    margin-right: 0px;
    opacity: 0.9;
`;

const NavAnchor = styled.a` 
    color: #f2f2f2;
    font-family: Helvetica;
    font-weight: 300;
    min-width: 30px;
    text-align: center;
    padding: 14px 50px;
    text-decoration: none;
    font-size: 17px;
    &:hover {
        background-color: #ddd;
        color: black;
    }
`;

const TopNav = () => (
     <NavWrapper>
        <Link href="/" passHref>
            <NavAnchor>Home</NavAnchor>
        </Link>
        <Link href="/favorites" passHref>
            <NavAnchor>Liked</NavAnchor>
        </Link>
    </NavWrapper>
);

export default TopNav;