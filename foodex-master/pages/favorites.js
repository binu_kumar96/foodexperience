import React from "react";
import fetch from "isomorphic-unfetch";
import axios from "axios";
import Router from "next/router";
import Layout from "./components/layout";
import styled from "styled-components";
import { DeleteOutline } from "@styled-icons/material-twotone/DeleteOutline";

const RedDeleteOutline= styled(DeleteOutline)`
  color: Red;
`;

const Heading = styled.h2`
    font-family: Helvetica;
    margin-bottom: 15px;
    font-weight: 500;
    color: #FF73FB;
    font-size: 40px;
`;

const Card = styled.div`
    box-shadow: 0 1px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    margin-bottom: 8px;
    width: 100%;   
    @media screen and (min-width: 480px) {
        width: 60%;
    }
    &:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }
`;

const FavoriteContainer = styled.div`
    display: flex;
    flex-direction: row
    padding: 10px;
    width: 100%;
`;

const FavoriteImage = styled.img`
    width: 15%;
    @media screen and (max-width: 480px) {
        width: 20%;
    }
`;

const FavoriteText = styled.div`
    font-family: Helvetica;
    color: #4856FE;
    margin: auto;
    padding: 15px;
    font-size: 14px;
    font-weight: 400;
    @media screen and (min-width: 480px) {
        font-size: 15px;
        padding: 15px;
        margin-top: 10px;       
    }
`;

const Button = styled.a`
    margin-right: 1rem;
    margin-top: 18px;
    position: relative;
    justify-content: 'flex-end';
    @media screen and (min-width: 480px) {
        margin-right: 1rem;
        margin-top: 15px;
    }    
}
`;

const EmptyContent = styled.div`
    display: none;
}
`;

function removeFromFavorites(index) {
    const body = {
        "userId": index
    };

    axios.post(`http://localhost:8000/remove`, body, {
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
        }
    }).then(res => {
        console.log(res);
        Router.reload();
    });
}

const handler = (index) => (
    removeFromFavorites(index)
);

const Favourites = ({ data }) => (
    <Layout>
        <Heading>Favorites</Heading>
        <>
            {
                data.map((content, index) => content["favourite"] === true ? (
                        <Card key={content["uid"]}>
                            <FavoriteContainer>
                                <FavoriteImage src={content["post_img"]} alt="Post"/>
                                <FavoriteText>{content["name"]}'s Post</FavoriteText>
                                <Button onClick={() => handler(content["uid"])}>
                                    <RedDeleteOutline size="35" />
                                </Button>
                            </FavoriteContainer>
                        </Card>
                    ) : (
                        <EmptyContent key={index} />
                    )
                )
            }
        </>
    </Layout>
);

export async function getServerSideProps() {
    // Fetch data from external API
    const res = await fetch(`http://localhost:8000/`);
    const data = await res.json();

    // Pass data to the page via props
    return { props: { data } }
}

export default Favourites;