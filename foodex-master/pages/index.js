import styled from "styled-components";
import Layout from "./components/layout";
import fetch from "isomorphic-unfetch";
import axios from "axios";
import Router from "next/router";
import { Heart } from "@styled-icons/evil/Heart";
import { HeartFill } from "@styled-icons/octicons/HeartFill";
import { Comment } from "@styled-icons/evil/Comment";
import { ShareApple } from "@styled-icons/evil/ShareApple";
import { SuitHeartFill } from "@styled-icons/bootstrap/SuitHeartFill";

const DarkHeart = styled(Heart)`
  color: black;
`;

const RedHeartFill = styled(HeartFill)`
  color: Red;
`;

const DarkComment= styled(Comment)`
  color: black;
`;

const DarkShareApple= styled(ShareApple)`
  color: black;
`;

const RedSuitHeartFill= styled(SuitHeartFill)`
  color: black;
`;

const Heading = styled.h2`
    font-family: Helvetica;
    margin-bottom: 15px;
    font-weight: 500;
    color: #FF73FB;
    font-size: 40px;
`;

const Card = styled.div`
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    margin-bottom: 25px;
    width: 100%;
    @media screen and (min-width: 480px) {
      width: 60%;
    }
    &:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }
`;

const ProfileContainer = styled.div`
    display: flex;
    padding: 12px;
    width: 100%
`;

const ProfileImage = styled.img`
    border-radius: 50%;
    width: 10%;
`;

const ProfileText = styled.div`
    font-family: Helvetica;
    color: #4856FE;
    padding: 10px;
    font-weight: 600;
    @media screen and (min-width: 480px) {
      font-size: 15px;
      padding: 15px;
    }
`;

const ActivitiesContainer = styled.div`
    padding: 2px 16px;
`;

const PostImage = styled.img`
    width: 100%;
`;

const ButtonContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: left;
    margin-left: 0px;
`;

const Button = styled.div`
    padding: 0px 7px 0px 0px;
    
`;

const LikeButton = styled.a`
    padding: 0px 7px 0px 0px;
    
`;
const LikedContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: left;
    align-items: center;
    margin-top: 10px;
    margin-left: 8px;
`;

const LikedText = styled.div`
    font-family: Helvetica;
    font-weight: 800;
    font-size: 14px;
    margin-left: 6px;
    color: #4856FE;
`;

const DescriptionContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: left;
    align-items: center;
    margin-top: 10px;
    margin-left: 8px;
    font-family: Helvetica;
    font-weight: 400;
    font-size: 12px;
    color: #000000;
`;

const HashTagContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: left;
    align-items: center;
    margin-top: 5px;
    margin-left: 8px;
    font-family: Helvetica;
    font-weight: 200;
    font-size: 12px;
    color: #4856FE;
`;

const CommentContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: left;
    align-items: center;
    margin-top: 10px;
    margin-left: 8px;
    margin-bottom: 10px;
    font-family: Helvetica;
    font-weight: 800;
    font-size: 12px;
    color: #A8A8A8;
`;

function addToFavorites(index) {
    const body = {
        "userId": index
    };

    axios.post(`http://localhost:8000/favorites`, body, {
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
        }
    }).then(res => {
        console.log(res);
        Router.reload();
    });
}

const handler = (index) => (
    addToFavorites(index)
);

const Home = ({ data }) => (
     <Layout>
        <Heading>News Feed</Heading>
        {
            data.map((content) => (
                    <Card key={content["uid"]}>
                        <ProfileContainer>
                            <ProfileImage src={content["profile_img"]} alt="Profile"/>
                            <ProfileText>{content["name"]}</ProfileText>
                        </ProfileContainer>
                        <PostImage src={content["post_img"]} alt="Post"/>
                        <ActivitiesContainer>
                            <ButtonContainer>
                                {
                                    (content["favourite"] === true ? (
                                        <LikeButton onClick={() => handler(content["uid"])}>
                                            <RedHeartFill size="30"/>
                                        </LikeButton>
                                    ) : (
                                        <LikeButton onClick={() => handler(content["uid"])}>
                                            <DarkHeart size="32"/>
                                        </LikeButton>
                                    ))
                                }
                                <Button>
                                    <DarkComment size="30"/>
                                </Button>
                                <Button>
                                    <DarkShareApple size="30"/>
                                </Button>
                            </ButtonContainer>
                            <LikedContainer>
                                <RedSuitHeartFill size="14"/><LikedText>{content["likes"]} Likes</LikedText>
                            </LikedContainer>
                            <DescriptionContainer>{content["description"]}</DescriptionContainer>
                            <HashTagContainer>{content["hash_tags"]}</HashTagContainer>
                            <CommentContainer>View {content["comments"]} Comments</CommentContainer>
                        </ActivitiesContainer>
                    </Card>
                )
            )
        }
    </Layout>
);

export async function getServerSideProps() {
    // Fetch data from external API
    const res = await fetch(`http://localhost:8000/`);
    const data = await res.json();

    // Pass data to the page via props
    return { props: { data } }
}

export default Home;