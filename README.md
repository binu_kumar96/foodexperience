# FoodExperience

This is a 'Mobile-First' single paged food blogging Application named `"Food Experience"`.

### Contents 
1. Functional Specification
2. Technical Specification
    * `foodex-backend` Specification
    * `foodex-master` Specification
3. How to Run
4. Screenshots of the Application
    * Mobile View
    * Web View

---

## Functional Specification

Key features that functions in this respective application are,

1. A news feed where a user can add a post to his/her's favorite list.
2. A favorite list where a user can view and remove his/her's favorite items.
3. Navigation responsiveness (when switching to desktop view).

---

## Technical Specification

This project contains two main components,

1. `foodex-backend`, developed using `Node.js` and `express.js`, which acts as the main server handling API requests.
2. `foodex-master`, developed using `Next.js` along with `React.js` and `styled-components`, handles the front-end components. 

Note: Following diagram represents the high-level architecture of the application.

![High Level Design](image-resources/high-level-architecture-FoodEx.jpg)

### `foodex-backend` Specification

There are three main components, `index.js`, `./controller/controller.js` and `./assets/posts.json`,

1. `index.js` initializes the services and routes of the api calls.
2. `./controller/controller.js` will work as a controller class in handling all the data manipulations in regards of the services.
3. `./assets/posts.json` is used as a data storage component.

### `foodex-master` Specification

There are three main components, pages, components and images,

1. Pages consists of two pages (`index.js` and `favorites.js`) and one `styled-components` configuration (`_document.js`).
    1. `index.js` is the view point of home page.
    2. `favorites.js` is the view point of liked page.
    3. `_document.js` configures the browser to pick the `styled-components` each time page loads.
2. Components consists of all the common elements, `header.js`, `bottom-nav.js`, `top-nav.js` and `layout.js`.
    1. `header.js` contains the head component of the page.
    2. `bottom-nav.js` contains the bottom navigation (mobile view) of the page.
    3. `top-nav.js` contains the top navigation (web view) of the page.
    4. `layout.js` creates the structure of the page.
3. Images consist of two folders, `feed` and `profiles`, images are stored in the front-end but url's are served from a request.
    1. `feed` contains all the images related to the posts.
    2. `profiles` contains all the images related to the profile pictures.
           
Note:

1. Styling is done using `styled-components`.
2. Get requests are served by `getServerSideProps()`, function.
3. axios handles all the **POST** requests.
4. A react hook is defined to get the window width in order to determine the navigation placement.
5. Posts resize based on the window size.
  
---

## How to Run

1. Firstly, make sure that node.js is installed on your PC. (Check using the terminal `node --version`, if not installed visit: https://nodejs.org/en/download/, and install the latest version). 
2. Clone the project to your desired location using the command: `git clone https://bitbucket.org/binu_kumar96/foodexperience.git`.
3. Go to the root directory of `foodexperience/foodex-master` and install all the node dependencies using the command: `npm install`.
4. Then go to the root of `foodex-backend`, and start the server using the command: `node index.js` (will serve on http://localhost:8000/).
5. Lastly, go to the root of `foodex-master`, and start the app using the command: `npm run dev` (will run on http://localhost:3000/).

Note: Sadly, there won't be any initial data setup (i.e, database config), since it is handled using a JSON component.

---

## Screenshots of the Application

#### Mobile View

Home Page            |  Liked Page
:-------------------------:|:-------------------------:
![Mobile Home](image-resources/mobile-home.png)  |  ![Mobile Home](image-resources/mobile-liked.png)

#### Web View

Home Page            |  Liked Page
:-------------------------:|:-------------------------:
![Web Home](image-resources/web-home.png)  |  ![Web Home](image-resources/web-liked.png)

Cheers!!!